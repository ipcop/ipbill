/*
 * IPBill helper program - setipbillaccess
 *
 * Copyright (c) 2005, Reto Buerki <reet (at) codelabs.ch>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * This program is distributed under the terms of the GNU General Public
 * Licence.  See the file COPYING for details.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "setuid.h"

FILE *fwdfile = NULL;
FILE *ifacefile = NULL;

void exithandler(void)
{
	if (fwdfile)
		fclose(fwdfile);
}

int main(void)
{
	char s[STRING_SIZE] = "";
	char iface[STRING_SIZE] = "";
	int count;
	char *srcip;
	char *enabled;
	char *result;
	char command[STRING_SIZE];

	if (!(initsetuid()))
		exit(1);

	atexit(exithandler);

	/*  read external interface name */
	if (!(ifacefile = fopen("/var/ipcop/red/iface", "r")))
	{
		fprintf(stderr, "[!] Couldn't open iface file\n");
		exit(1);
	}
	if (fgets(iface, STRING_SIZE, ifacefile))
	{
		if (iface[strlen(iface) - 1] == '\n')
			iface[strlen(iface) - 1] = '\0';
	}
	fclose (ifacefile);
	if (!VALID_DEVICE(iface))
	{
		fprintf(stderr, "[!] Bad iface: %s\n", iface);
		exit(1);
	}

	if (!(fwdfile = fopen("/var/ipcop/ipbill/hosts", "r")))
	{
		fprintf(stderr, "[!] Couldn't open ipbill hosts file\n");
		exit(1);
	}

	/* flush IPBill chains */
	safe_system("/sbin/iptables -F IPBILL_I");
	safe_system("/sbin/iptables -F IPBILL_F");

	while (fgets(s, STRING_SIZE, fwdfile) != NULL)
	{
		if (s[strlen(s) - 1] == '\n')
			s[strlen(s) - 1] = '\0';
		count = 0;
		srcip = NULL;
		enabled = NULL;
		result = strtok(s, ",");
		while (result)
		{
			if (count == 1)
				srcip = result;
			else if (count == 4)
				enabled = result;
			count++;
			result = strtok(NULL, ",");
		}

		if (!(srcip && enabled))
			break;

		if (!VALID_IP(srcip))
		{
			fprintf(stderr, "Bad source IP: %s\n", srcip);
			exit(1);
		}

		if (strcmp(enabled, "off") == 0)
		{
            /* add INPUT rule */
			memset(command, 0, STRING_SIZE);
			snprintf(command, STRING_SIZE - 1, "/sbin/iptables -I IPBILL_I -s %s -j DROP", srcip);
			safe_system(command);
			/* add FORWARD rule */
			memset(command, 0, STRING_SIZE);
			snprintf(command, STRING_SIZE - 1, "/sbin/iptables -I IPBILL_F -s %s -o %s -j DROP", srcip, iface);
			safe_system(command);

		}
	}
	return 0;
}
