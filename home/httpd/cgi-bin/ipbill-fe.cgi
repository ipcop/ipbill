#!/usr/bin/perl
#
# IPBill, a traffic billing system for IPCop
# Copyright (c) 2005, Reto Buerki <reet (at) codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.
#
use strict;

# enable only the following on debugging purpose
#use warnings;
use CGI::Carp 'fatalsToBrowser';

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

# files where we store our data
my $setting = "${General::swroot}/ipbill/settings";
our $hostfile = "${General::swroot}/ipbill/hosts";
our $groupfile = "${General::swroot}/ipbill/groups";

my %settings = ();
my %calcs = ();

$settings{'ACTION'} = '';
$settings{'KEY1'} = '';
$settings{'KEY2'} = '';
$settings{'PRICE'} = '';

# get GUI values
&Header::getcgihash(\%settings);

my $errormessage = '';
my $timer = '';

# load data
our @current_hosts = ();
if(open(FILE, "$hostfile")) {
	@current_hosts = <FILE>;
	close (FILE);
}
our @current_groups = ();
if(open(FILE, "$groupfile")) {
	@current_groups = <FILE>;
	close (FILE);
}

# read settings
&General::readhash($setting, \%settings);

##[funcs]#######################################################################

if ($settings{'ACTION'} eq $Lang::tr{'toggle enable disable'}) {
	chomp(@current_hosts[$settings{'KEY1'}]);
	my @temp = split(/\,/,@current_hosts[$settings{'KEY1'}]);
	my $K2 = $settings{'KEY2'};
	# toggle button and activate/deactivate specific rule
	if ($temp[$K2] eq 'on') {
		$temp[$K2] = 'off';
		# read startttime and calc mins to pay
		my $start = $temp[5];
		my $now = time;
		my $mins = int(($now - $start) / 60);
		# write result
		$temp[6] = $mins;
		$temp[7] = sprintf("%.1f", $mins * $settings{'PRICE'});
		#$temp[6] = &round($mins * $settings{'PRICE'});
		# reset starttime
		$temp[5] = 0;
	}
	elsif ($temp[$K2] eq 'off') {
		$temp[$K2] = 'on';
		$temp[5] = time;
		undef($temp[6]);
		undef($temp[7]);
	}
	else {
		$errormessage = "Unknown state: $temp[ $K2 ], this should not happen!!!";
	}
	@current_hosts[$settings{'KEY1'}] = join (',',@temp)."\n";
	$settings{'KEY1'} = '';
	&WriteHostData;
	# execute helper
	system("/usr/local/bin/setipbillaccess");
}

if ($settings{'ACTION'} eq $Lang::tr{'toggle enable disable group'}) {
	# KEY1 = group index
	# KEY2 = state
	my $K1 = $settings{'KEY1'};
	my $K2 = $settings{'KEY2'};

	chomp(@current_groups[$K1]);
	my @temp = split(/\,/,@current_groups[$K1]);
	if ($K2 eq 'off') {
		# activate all hosts in this group
		$temp[0] = 'on';
		my $key = -1;
		my @host;
		foreach my $line (@current_hosts) {
			@host = split(/\,/,$line);
			$key++;
			next unless $host[2] eq $temp[1];
			$host[4] = 'on';
			$host[5] = time;
			undef($host[6]);
			undef($host[7]);
			@current_hosts[$key] = join (',',@host)."\n";
		}
	} else {
		# deactivate all hosts in this group
		$temp[0] = 'off';
		my $key = -1;
		my @host;
		foreach my $line (@current_hosts) {
			@host = split(/\,/,$line);
			$key++;
			next unless $host[2] eq $temp[1];
			$host[4] = 'off';
			# read startttime and calc mins to pay
			my $start = $host[5];
			my $now = time;
			my $mins = int(($now - $start) / 60);
			# write result
			$host[6] = $mins;
			$host[7] = sprintf("%.1f", $mins * $settings{'PRICE'});
			# reset starttime
			$host[5] = 0;
			@current_hosts[$key] = join (',',@host)."\n";
		}
	}
	@current_groups[$K1] = join (',',@temp)."\n";
	$settings{'KEY1'} = '';
	$settings{'KEY2'} = '';
	&WriteGroupData;
	&WriteHostData;
	system("/usr/local/bin/setipbillaccess");
}



##[MAIN]########################################################################


&Header::showhttpheaders();

# html stuff
print <<END
<html>
<head>
 <meta http-equiv="refresh" content="60; URL="ipbill-fe.cgi">
 <title>IPBill: Host Control</title>
 </head>
<body>
<br>
<fieldset>
<table width='100%'>
<tr>
	<td><img src='/images/header_ipbill.png' alt='IPBill'></td>
</tr>
<tr>
    <td style="border:1px solid black;" bgcolor="#d7d8e8">
END
;

&Header::openbigbox('100%', 'center');

# error status message
if ($errormessage) {
	&Header::openbox('100%', 'left', $Lang::tr{'error messages'});
	print "<FONT CLASS='base'>$errormessage</FONT>\n";
	&Header::closebox();
}

# functions box
&Header::openbox('100%', 'left', $Lang::tr{'functions'});
print <<END
<table width='100%'>
<tr>
	<td width='20%'>$Lang::tr{'price per minute'}</td>
	<td>$settings{'PRICE'}</td>
</tr>
<tr>
	<td></td>
	<td valign='top' style='font-size:11px;'>
	 (<a href="ipbill-fe.cgi">$Lang::tr{'refresh'}</a>)
	 (<a href="ipbill.cgi">$Lang::tr{'change'}</a>)
	</td>
</tr>
</table>
END
;
&Header::closebox();
# host control box
&Header::openbox('100%', 'left', $Lang::tr{'host controllcenter'});
print <<END
<table width='100%' cellspacing='3' cellpadding='3'>
<tr>
	<th></th>
	<th>$Lang::tr{'hostname'}<div style="font-size:9px;">$Lang::tr{'name of client'}</div></th>
    <th>$Lang::tr{'groupname'}<div style="font-size:9px;">$Lang::tr{'group of client'}</div></th>
	<th>$Lang::tr{'remark'}<div style="font-size:9px;">$Lang::tr{'descr of client'}</div></th>
	<th>$Lang::tr{'time'}<div style="font-size:9px;">[mins]</div></th>
	<th>$Lang::tr{'price'}<div style="font-size:9px;">$Lang::tr{'totalprice'}</div></th>
	<th>$Lang::tr{'timer'}<div style="font-size:9px;">[h:m:s]</div></th>
	<th>$Lang::tr{'action'}<div style="font-size:9px;">$Lang::tr{'action_text'}</div></th>
</tr>
END
;
my $key = 0;
foreach my $line (@current_hosts) {
	chomp($line);
	my @temp = split(/\,/,$line);

	# color lines
	if ($key % 2) {
		print "<tr bgcolor='${Header::table2colour}'>";
	} else {
		print "<tr bgcolor='${Header::table1colour}'>";
	}

	# get correct icon for entry
	my $state_gif = '';
	my $gif = '';
	my $gdesc = '';
	if ($temp[4] eq "on") {
		$state_gif = 'enabled.png';
		$gif = 'on.gif';
		$gdesc = $Lang::tr{'click to disable'};
	} else {
		$state_gif = 'disabled.png';
		$gif = 'off.gif';
		$gdesc = $Lang::tr{'click to enable'};
	}
	if ($temp[5] eq '0') {
		$timer = &TimeFromSec(0);
	} else {
		# calc timer
		my $t = time - $temp[5];
		$timer = &TimeFromSec($t);
	}
	if ($temp[2] eq 'None') {
		$temp[2] = '-';
	}
	if ($temp[6] eq '') {
		$temp[6] = '-';
	}
	if ($temp[7] eq '') {
		$temp[7] = '-';
	}

	print <<END
<td width='5%' class='base'><img src='/images/$state_gif' alt='Status' title='Status'></td>
<td width='25%' class='base'>$temp[0]</td>
<td width='15%' class='base'>$temp[2]</td>
<td width='25%' class='base'>$temp[3]</td>
<td width='10%' align='center' class='base'>$temp[6]</td>
<td width='10%' align='center' style='color:red;'>$temp[7]</td>
<td width='10%' align='center' class='base'>$timer</td>
<td align='center' width='5%'>
<form method='post' name="hosts" action='$ENV{'SCRIPT_NAME'}'>
<input type='hidden' name='ACTION' value='$Lang::tr{'toggle enable disable'}'>
<input type='image' name='$Lang::tr{'toggle enable disbale'}' src='/images/$gif'
       alt='$gdesc' title='$gdesc'>
<input type='hidden' name='KEY1' value='$key'>
<input type='hidden' name='KEY2' value='4'>
<input type='hidden' name='IP' value='$temp[1]'>
</form>
</tr>
END
;
	$key++;
}
print "</table>";

# if we have entries, display legend
if ($key) {
	print <<END
<table>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'legend'}:&nbsp;</b></td>
	<td><img src='/images/on.gif' alt='$Lang::tr{'click to disable'}'></td>
	<td class='base'>$Lang::tr{'click to disable'}</td>
	<td>&nbsp;&nbsp;</td>

	<td><img src='/images/off.gif' alt='$Lang::tr{'click to enable'}'></td>
	<td class='base'>$Lang::tr{'click to enable'}</td>
</tr>
</table>
END
;
}

&Header::closebox();

# group control box
&Header::openbox('100%', 'left', $Lang::tr{'group controllcenter'});
print <<END
<table width='100%' cellspacing='3' cellpadding='3'>
<tr>
	<th></th>
    <th>$Lang::tr{'groupname'}<div style="font-size:9px;">$Lang::tr{'group of client'}</div></th>
	<th>$Lang::tr{'remark'}<div style="font-size:9px;">$Lang::tr{'descr of group'}</div></th>
	<th>$Lang::tr{'action'}<div style="font-size:9px;">$Lang::tr{'action_text'}</div></th>
</tr>
END
;
my $key = 0;
foreach my $line (@current_groups) {
	chomp($line);
	my @temp = split(/\,/,$line);

	# color lines
	if ($key % 2) {
		print "<tr bgcolor='${Header::table2colour}'>";
	} else {
		print "<tr bgcolor='${Header::table1colour}'>";
	}

	# get correct icon for entry
	my $state_gif = '';
	my $gif = '';
	my $gdesc = '';
	if ($temp[0] eq "on") {
		$state_gif = 'enabled.png';
		$gif = 'on.gif';
		$gdesc = $Lang::tr{'click to disable'};
	} else {
		$state_gif = 'disabled.png';
		$gif = 'off.gif';
		$gdesc = $Lang::tr{'click to enable'};
	}

	print <<END
<td width='5%' class='base'><img src='/images/$state_gif' alt='Status' title='Status'></td>
<td width='40%' class='base'>$temp[1]</td>
<td width='55%' class='base'>$temp[2]</td>
<td align='center' width='5%'>
<form method='post' name="hosts" action='$ENV{'SCRIPT_NAME'}'>
<input type='hidden' name='ACTION' value='$Lang::tr{'toggle enable disable group'}'>
<input type='image' name='$Lang::tr{'toggle enable disbale group'}' src='/images/$gif'
       alt='$gdesc' title='$gdesc'>
<input type='hidden' name='KEY1' value='$key'>
<input type='hidden' name='KEY2' value='$temp[0]'>
</form>
</tr>
END
;
	$key++;
}
print "</table>";

# if we have entries, display legend
if ($key) {
	print <<END
<table>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'legend'}:&nbsp;</b></td>
	<td><img src='/images/on.gif' alt='$Lang::tr{'click to disable'}'></td>
	<td class='base'>$Lang::tr{'click to disable'}</td>
	<td>&nbsp;&nbsp;</td>

	<td><img src='/images/off.gif' alt='$Lang::tr{'click to enable'}'></td>
	<td class='base'>$Lang::tr{'click to enable'}</td>
</tr>
</table>
END
;
}

&Header::closebox();

&Header::closebigbox();

print <<END
	</td>
</tr>
</table>
</fieldset>
</body>
</html>
END
;

##[subs]########################################################################
sub WriteHostData {
    open(FILE, ">$hostfile") or die 'ipbill data file error';
    print FILE @current_hosts;
    close (FILE);
}
sub WriteGroupData {
    open(FILE, ">$groupfile") or die 'ipbill data file error';
    print FILE @current_groups;
    close (FILE);
}
sub TimeFromSec {
    my $timer = '';
    my $sec = 0.0;
    my $min = 0.0;
    my $hour = 0.0;
    my $time = shift;
    if ($time >= 60) {
        $min = int($time / 60);
        $sec = $time % 60;
        if ($min >= 60) {
            $hour = int($time / (60*60));
            $min = $min % 60;
        }
    } else {
        $sec = $time;
    }
    $timer = sprintf("%2.0f:%2.0f:%2.0f", $hour,$min,$sec);
    return($timer);
}
sub round {
    my($number) = shift;
    return int($number + .5);
}
################################################################################

