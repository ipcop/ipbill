#!/usr/bin/perl
#
# IPBill, a traffic billing system for IPCop
# Copyright (c) 2005, Reto Buerki <reet (at) codelabs.ch>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA  02110-1301, USA.
#
use strict;

# enable only the following on debugging purpose
#use warnings;
use CGI::Carp 'fatalsToBrowser';

require '/var/ipcop/general-functions.pl';
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";

# files where we store our data
my $setting = "${General::swroot}/ipbill/settings";
our $hostfile = "${General::swroot}/ipbill/hosts";
our $groupfile = "${General::swroot}/ipbill/groups";

my %settings = ();

$settings{'ACTION'} = '';
$settings{'KEY1'} = '';

$settings{'HOSTNAME'} = '';
$settings{'IP'} = '';
$settings{'DESCR'} = '';
$settings{'PRICE'} = '';
$settings{'ALLOW'} = '';
$settings{'GROUP'} = '';

# not saved to settings
my @nosaved = ('ACTION', 'KEY1', 'HOSTNAME', 'IP', 'DESCR', 'ALLOW', 'GROUP');

# get GUI values
&Header::getcgihash(\%settings);

my $errormessage = '';

# load data
our @current_hosts = ();
if(open(FILE, "$hostfile")) {
	@current_hosts = <FILE>;
	close (FILE);
}
our @current_groups = ();
if(open(FILE, "$groupfile")) {
	@current_groups = <FILE>;
	close (FILE);
}

##[funcs]#######################################################################
if ($settings{'ACTION'} eq $Lang::tr{'remove'}) {
	splice (@current_hosts,$settings{'KEY1'},1);
	open(FILE, ">$hostfile") or die 'ipbill hostfile error';
	print FILE @current_hosts;
	close(FILE);
	$settings{'KEY1'} = '';
	system("/usr/local/bin/setipbillaccess");
	&General::log("Removed host $settings{'HOSTNAME'} for IPBill");
	&WriteHostData;
}

if ($settings{'ACTION'} eq $Lang::tr{'save'}) {
	if ($settings{'PRICE'} eq '') {
		$errormessage = "No valid price per minute defined!";
	}
	if (!($settings{'PRICE'} =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/)) {
		$errormessage = "$settings{'PRICE'} is not a valid price!";
	}
	unless ($errormessage) {
		# save price to settings
		map(delete ($settings{$_}), (@nosaved));
		&General::writehash($setting, \%settings);
	}
}

if ($settings{'ACTION'} eq $Lang::tr{'add'}) {

	if ($settings{'HOSTNAME'} eq '') {
		$errormessage = "Please provide a valid hostname!";
	}

	if (!&General::validipormask($settings{'IP'}))
	{
		$errormessage = "Given IP address is not valid!";
	}

	if ($settings{'GROUP'} eq '') {
		$settings{'GROUP'} = 'None';
	}

	unless ($errormessage) {
		if ($settings{'DESCR'} eq '') {
				$settings{'DESCR'} = 'None';
		}
		unshift(@current_hosts, "$settings{'HOSTNAME'},$settings{'IP'},$settings{'GROUP'},$settings{'DESCR'},$settings{'ALLOW'},0\n");
		&WriteHostData;
		system("/usr/local/bin/setipbillaccess");
		&General::log("Added host $settings{'HOSTNAME'} for IPBill");
		# clear entries
		map ($settings{$_} = '', @nosaved);
	}
}

if ($settings{'ACTION'} eq $Lang::tr{'add group'}) {

	if ($settings{'GROUP'} eq '') {
		$errormessage = "Please provide a valid groupname!";
	}

	unless ($errormessage) {
		if ($settings{'DESCR'} eq '') {
				$settings{'DESCR'} = 'None';
		}
		unshift(@current_groups, "off,$settings{'GROUP'},$settings{'DESCR'},0\n");
		&WriteGroupData;
		&General::log("Added group $settings{'GROUP'} for IPBill");
		# clear entries
		map ($settings{$_} = '', @nosaved);
	}
}

if ($settings{'ACTION'} eq $Lang::tr{'remove group'}) {
	splice (@current_groups,$settings{'KEY1'},1);
	open(FILE, ">$groupfile") or die 'ipbill groupfile error';
	print FILE @current_groups;
	close(FILE);
	$settings{'KEY1'} = '';
	&General::log("Removed group $settings{'GROUP'} for IPBill");
	&WriteGroupData;
}


##[MAIN]########################################################################


&General::readhash($setting, \%settings);

&Header::showhttpheaders();

&Header::openpage('IPBill', 1, '');

&Header::openbigbox('100%', 'left');

# error status message
if ($errormessage) {
	&Header::openbox('100%', 'left', $Lang::tr{'error messages'});
	print "<FONT CLASS='base'>$errormessage</FONT>\n";
	&Header::closebox();
}

# define price bos
&Header::openbox('100%', 'left', $Lang::tr{'general'});
print <<END
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<table width='100%'>
<tr>
	<td width='25%' class='base'>$Lang::tr{'price per minute'}&nbsp;</td>
	<td>
	 <input type='text' name='PRICE' value='$settings{'PRICE'}'>
	</td>
</tr>
<tr>
	<td>Frontend:&nbsp;</td>
	<td><a href="ipbill-fe.cgi">$Lang::tr{'open'}</a></td>
</tr>
<tr>
	<td colspan='2' align='center'><hr />
	 <input type='submit' name='ACTION' value='$Lang::tr{'save'}'>
</tr>
</table>
</form>
END
;

&Header::closebox();

# add host box
&Header::openbox('100%', 'left', $Lang::tr{'add host'});
print <<END
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<table width='100%'>
<tr>
	<td width='25%' class='base'>$Lang::tr{'hostname'}:&nbsp;</td>
	<td>
	 <input type='text' name='HOSTNAME' value='$settings{'HOSTNAME'}'>
	</td>
</tr>
<tr>
	<td class='base'>$Lang::tr{'ip address'}:&nbsp;</td>
	<td>
	 <input type='text' name='IP' value='$settings{'IP'}'>
	</td>
</tr>
<tr>
	<td class='base'>$Lang::tr{'groupname'}:&nbsp;</td>
	<td>
	 <select name='GROUP' size='3'>
END
;
# display groups to select
foreach my $line (@current_groups) {
	chomp($line);
	my @temp = split(/\,/,$line);
	print "<option>$temp[1]</option>";
}
print <<END
	 </select>
	</td>
</tr>
<tr>
	<td class='base'>$Lang::tr{'remark'}:&nbsp;<img src='/blob.gif' alt='*'></td>
	<td>
	 <input type='text' maxlength='30' name='DESCR' value='$settings{'DESCR'}'>
         <input type='hidden' name='ALLOW' value='off'>
	</td>
</tr>
</table>
<hr>
<table width='100%'>
<tr>
	<td class='base' width='50%'><img src='/blob.gif' align='top' alt='*'>
	&nbsp;$Lang::tr{'this field may be blank'}</td>
	<td width='50%' align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'add'}'>
	</td>
</tr>
</table>
</form>
END
;

&Header::closebox();

# existing host box
&Header::openbox('100%', 'left', $Lang::tr{'current hosts'});
print <<END
<table width='100%'>
<tr>
	<th>$Lang::tr{'hostname'}</th>
	<th>$Lang::tr{'ip address'}</th>
	<th>$Lang::tr{'groupname'}</th>
	<th>$Lang::tr{'remark'}</th>
	<th>$Lang::tr{'action'}</th>
</tr>
END
;
my $key = 0;
foreach my $line (@current_hosts) {
	chomp($line);
	my @temp = split(/\,/,$line);

	# color lines
	if ($key % 2) {
		print "<tr bgcolor='${Header::table2colour}'>";
	} else {
		print "<tr bgcolor='${Header::table1colour}'>";
	}

	print <<END

<td class='base'>$temp[0]</td>
<td class='base'>$temp[1]</td>
<td class='base'>$temp[2]</td>
<td class='base'>$temp[3]</td>

<td align='center'>
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<input type='hidden' name='ACTION' value='$Lang::tr{'remove'}'>
<input type='image' name='$Lang::tr{'remove'}' src='/images/delete.gif'
       alt='$Lang::tr{'remove'} title='$Lang::tr{'remove'}'>
<input type='hidden' name='KEY1' value='$key'>
</form>
</tr>
END
;
	$key++;
}
print "</table>";

# if we have entries, display legend
if ($key) {
	print <<END
<table>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'legend'}:&nbsp;</b></td>
	<td><img src='/images/delete.gif' alt='$Lang::tr{'remove'}'></td>
	<td class='base'>$Lang::tr{'remove'}</td>

</tr>
</table>
END
;
}

&Header::closebox();

# add group box
&Header::openbox('100%', 'left', $Lang::tr{'add group'});
print <<END
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<table width='100%'>
<tr>
	<td width='25%' class='base'>$Lang::tr{'groupname'}:&nbsp;</td>
	<td>
	 <input type='text' name='GROUP' value='$settings{'GROUP'}'>
	</td>
</tr>
<tr>
	<td class='base'>$Lang::tr{'remark'}:&nbsp;<img src='/blob.gif' alt='*'></td>
	<td>
	 <input type='text' maxlength='30' name='DESCR' value='$settings{'DESCR'}'>
	</td>
</tr>
</table>
<hr>
<table width='100%'>
<tr>
	<td class='base' width='50%'><img src='/blob.gif' align='top' alt='*'>
	&nbsp;$Lang::tr{'this field may be blank'}</td>
	<td width='50%' align='center'>
	 <input type='submit' name='ACTION' value='$Lang::tr{'add group'}'>
	</td>
</tr>
</table>
</form>
END
;

&Header::closebox();


# existing groups box
&Header::openbox('100%', 'left', $Lang::tr{'manage groups'});
print <<END
<table width='100%'>
<tr>
	<th>$Lang::tr{'groupname'}</th>
	<th>$Lang::tr{'remark'}</th>
	<th>$Lang::tr{'action'}</th>
</tr>
END
;
my $key = 0;
foreach my $line (@current_groups) {
	chomp($line);
	my @temp = split(/\,/,$line);

	# color lines
	if ($key % 2) {
		print "<tr bgcolor='${Header::table2colour}'>";
	} else {
		print "<tr bgcolor='${Header::table1colour}'>";
	}

	print <<END

<td class='base'>$temp[1]</td>
<td class='base'>$temp[2]</td>

<td align='center'>
<form method='post' action='$ENV{'SCRIPT_NAME'}'>
<input type='hidden' name='ACTION' value='$Lang::tr{'remove group'}'>
<input type='image' name='$Lang::tr{'remove group'}' src='/images/delete.gif'
       alt='$Lang::tr{'remove group'} title='$Lang::tr{'remove group'}'>
<input type='hidden' name='KEY1' value='$key'>
</form>
</tr>
END
;
	$key++;
}
print "</table>";

# if we have entries, display legend
if ($key) {
	print <<END
<table>
<tr>
	<td class='boldbase'>&nbsp;<b>$Lang::tr{'legend'}:&nbsp;</b></td>
	<td><img src='/images/delete.gif' alt='$Lang::tr{'remove'}'></td>
	<td class='base'>$Lang::tr{'remove'}</td>

</tr>
</table>
END
;
}

&Header::closebox();

&Header::closebigbox();

&Header::closepage();

##[subs]########################################################################
sub WriteHostData {
	open(FILE, ">$hostfile") or die 'ipbill data file error';
	print FILE @current_hosts;
	close (FILE);
}
sub WriteGroupData {
	open(FILE, ">$groupfile") or die 'ipbill group file error';
	print FILE @current_groups;
	close (FILE);
}
################################################################################

